package com.superman.oops;

/**
 * 
 * @author jeev567
 * @Description: Abstraction is the process of abstraction in Java is used to
 *               hide certain details and only show the essential features of
 *               the object. In other words, it deals with the outside view of
 *               an object (interface). Now this is the part which confuses me
 *               always
 * @Note1: Encapsulation is data hiding(information hiding) while Abstraction is
 *         detail hiding(implementation hiding).
 * @Note2: While encapsulation groups together data and methods that act upon
 *         the data, data abstraction deals with exposing the interface to the
 *         user and hiding the details of implementation.
 */
// Main Class
public class Abstraction {

	public static void main(String[] args) {
		Shape s1 = new Circle("Red", 2.2);
		Shape s2 = new Rectangle("Yellow", 2, 4);

		System.out.println(s1.toString());
		System.out.println(s2.toString());
	}

}

// Abstract Class
abstract class Shape {

	String color;

	// These are abstract methods
	abstract double area();

	public abstract String toString();

	// Abstract class can have constructor
	public Shape(String color) {
		System.out.println("Shape constructor called");
		this.color = color;
	}

	// This is a concrete method
	public String getColor() {
		return color;
	}

}

// Extending Class
class Circle extends Shape {
	double radius;

	public Circle(String color, double radius) {

		// calling Shape constructor
		super(color);
		System.out.println("Circle constructor called");
		this.radius = radius;
	}

	@Override
	double area() {
		return Math.PI * Math.pow(radius, 2);
	}

	@Override
	public String toString() {
		return "Circle color is " + super.color + "and area is : " + area();
	}

}

// Extending class
class Rectangle extends Shape {

	double length;
	double width;

	public Rectangle(String color, double length, double width) {
		// calling Shape constructor
		super(color);
		System.out.println("Rectangle constructor called");
		this.length = length;
		this.width = width;
	}

	@Override
	double area() {
		return length * width;
	}

	@Override
	public String toString() {
		return "Rectangle color is " + super.color + "and area is : " + area();
	}

}
