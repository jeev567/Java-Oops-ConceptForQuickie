package com.superman.oops;

/**
 * 
 * @author jeev567
 * @Desscription Java - Encapsulation. Encapsulation is one of the four
 *               fundamental OOP concepts. The other three are inheritance,
 *               polymorphism, and abstraction. ... In encapsulation, the
 *               variables of a class will be hidden from other classes, and can
 *               be accessed only through the methods of their current
 *               class.herefore, it is also known as data hiding.
 * 
 * @Benifits: The fields of a class can be made read-only or write-only.
 * 			  A class can have total control over what is stored in its fields.
 * 
 * @IQ: 
 * 1.What is Encapsulation?
 * 2.What is the primary benefit of encapsulation?
 * 3....
 */
public class Encapsulation {

	private String name;
	private String idNum;
	private int age;

	public int getAge() {
		return age;
	}

	public String getName() {
		return name;
	}

	public String getIdNum() {
		return idNum;
	}

	public void setAge(int newAge) {
		age = newAge;
	}

	public void setName(String newName) {
		name = newName;
	}

	public void setIdNum(String newId) {
		idNum = newId;
	}
}
